import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { FormValidate } from './src/components/FormVaildate'
import Login from './src/components/Login'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Home from './src/components/Home';


export const baseURL = 'http://110.74.194.124:15011/v1/api/'
export const header = {
  'Content-Type': 'Application/json',
  'Authorization': 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
}

export default class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      username: ''
    }
  }
  async componentDidMount(){
    const username = await AsyncStorage.getItem('@username')
    this.setState({
      username: username
    })
    console.log(this.state)
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <Text>Add new text</Text>
        <Text>Add to develop brach</Text>
        {
          this.state.username != null ? <Home /> : <Login />
        }
      </View>
    )
  }
}
