/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import SignUp from './src/components/SignUp';

AppRegistry.registerComponent(appName, () => SignUp);
