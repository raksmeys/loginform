import React from 'react';
import { Button, StyleSheet, Dimensions, View } from 'react-native';
import { Form, TextValidator } from 'react-native-validator-form';
 
export class FormValidate extends React.Component {
    state = {
        email: '',
    }
 
    handleChange = (email) => {
        this.setState({ email });
    }
 
    submit = () => {
        // your submit logic
    }
 
    handleSubmit = () => {
        this.refs.form.submit();
    }
 
    render() {
        const { email } = this.state;
        return (
            <View style={styles.container}>
            <Form
                ref="form"
                onSubmit={this.handleSubmit}
            >
                <TextValidator
                    style={styles.inputStyle}
                    name="email"
                    label="email"
                    validators={['required', 'isEmail']}
                    errorMessages={['This field is required', 'Email invalid']}
                    placeholder="Your email"
                    type="text"
                    keyboardType="email-address"
                    value={email}
                    onChangeText={this.handleChange}
                />
                 <Button
                    style={styles.btn}
                    title="Submit"
                    onPress={this.handleSubmit}
                />
            </Form>
            </View>
        );
    }
}

const WIDTH = Dimensions.get('screen').width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputStyle: {
        width: WIDTH - 40,
        height: 70,
        borderRadius: 15,
        borderColor: 'lightgray',
        borderWidth: 0.5,
        marginTop: 10,
        paddingLeft: 10
    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        width: WIDTH - 40,
        height: 70,
        borderRadius: 15,
        backgroundColor: 'lightblue',
        marginTop: 10
    }
})