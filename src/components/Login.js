import React, { Component, useState } from 'react'
import { Text, View, StyleSheet, TextInput, Dimensions, TouchableOpacity, Image, KeyboardAvoidingView} from 'react-native'
// import {Form, TextValidator} from 'react-native-validator-form'
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Login extends Component {

    constructor(props){
        super(props)
        this.state = {
            username: '',
            email: '',
            pwd: '',
            errorUsername: '',
            errorEmail: '',
            errorPwd: ''
        }
    }
    handleLogin = async () => {
        console.log(this.state)
        // validate form input
        // Use regular expression
        // MARK: pathern to validate username: not blank, no number, no space, no special 
        let pattUsername = /[^a-z]/gi
        if (this.state.username == ""){
            this.setState({errorUsername: '* This field is required'})
        }
        else if (this.state.username.match(pattUsername) != null) {
            this.setState({errorUsername: `* INVALID USERNAME ${this.state.username.match(pattUsername)}`})
        }else if (this.state.username.match(pattUsername) == null){
            this.setState({errorUsername: ''})
            // after all validate, we will save data to storage  
            await AsyncStorage.setItem('@username', this.state.username)
        }
    }

    render() {
        return (
            <KeyboardAvoidingView 
                style={{flex: 1}}
                behavior={Platform.OS == "ios" ? "padding" : "height"}>
            <View style={styles.container}>
                <Image
                    style={{width: 100, height: 100, marginBottom: 10}}
                    resizeMode="contain" 
                    source={{uri: 'https://www.kshrd.com.kh/static/media/logo.f368c431.png'}}
                />
                <Text
                    style={{fontSize: 20, fontWeight: 'bold', marginBottom: 10}}
                >Korea software HRD Center</Text>
                <View style={{alignItems: 'flex-start'}}>
                    <Text style={{color: 'red'}}>{this.state.errorUsername}</Text>
                    <TextInput
                        style={styles.inputStyle} 
                        placeholder="Username"
                        onChangeText={(username) => {
                            this.setState({username})
                            
                        }}
                    />
                </View>
                {/* <TextInput 
                    style={styles.inputStyle} 
                    placeholder="Email"
                    onChangeText={(email) => this.setState({email})}
                /> */}
                <TextInput
                    style={styles.inputStyle}  
                    placeholder="Password"
                    secureTextEntry={true}
                    onChangeText={(pwd) => this.setState({pwd})}
                />
                <TouchableOpacity
                    onPress={this.handleLogin} 
                    style={styles.btn}>
                    <Text>Log In</Text>
                </TouchableOpacity>
            </View>
            </KeyboardAvoidingView>
        )
    }
}

const WIDTH = Dimensions.get('screen').width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputStyle: {
        width: WIDTH - 40,
        height: 70,
        borderRadius: 15,
        borderColor: 'lightgray',
        borderWidth: 0.5,
        marginTop: 10,
        paddingLeft: 10
    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        width: WIDTH - 40,
        height: 70,
        borderRadius: 15,
        backgroundColor: 'lightblue',
        marginTop: 10
    }
})
