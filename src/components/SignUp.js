import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions, TextInput, Image, TouchableOpacity } from 'react-native'
import {Form, Item, Picker, Icon} from 'native-base'
import { baseURL, header } from './../../App'

export default class SignUp extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            name: '',
            password: '',
            gender: '',
            photo: new FormData(),
            selected2: undefined
        }
    }

    onValueChange2(value) {
        this.setState({
            selected2: value
        });
    }

    registerUser = () => {
        console.log("hello register")
        // let urlProfile = new FormData()
        // urlProfile.append('photo', {type: 'image/png', uri: 'https://www.kshrd.com.kh/static/media/logo.f368c431.png', name: 'png'})
        let user = {
            'email': this.state.email,
            'name': this.state.name,
            'password': this.state.password,
            'gender': this.state.selected2,
            'photo': 'https://www.kshrd.com.kh/static/media/logo.f368c431.png'
        }

        fetch(`${baseURL}user?email=${this.state.email}&name=${this.state.name}&password=${this.state.password}&gender=${this.state.selected2}`, {
            method: 'POST',
            headers: header,
            body: JSON.stringify(user.photo)
        })
        .then(res => res.json())
        .then(res => console.log(res))
        
        
    }

    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={{width: 100, height: 100, marginBottom: 10}}
                    resizeMode="contain" 
                    source={{uri: 'https://www.kshrd.com.kh/static/media/logo.f368c431.png'}}
                />
                <Text
                    style={{fontSize: 20, fontWeight: 'bold', marginBottom: 10}}
                >Korea software HRD Center</Text>
                <TextInput
                    style={styles.inputStyle}
                    placeholder="Email"
                    onChangeText={(email) => this.setState({ email })}
                />
                <TextInput
                    style={styles.inputStyle}
                    placeholder="Name"
                    onChangeText={(name) => this.setState({ name })}
                />
                <TextInput
                    style={styles.inputStyle}
                    placeholder="Password"
                    secureTextEntry={true}
                    onChangeText={(password) => this.setState({ password })}
                />
                
                <View style={{alignItems: "flex-start"}}>
                <Text style={{marginTop: 10}}>Choose Gender:</Text>
                <Form>
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: WIDTH - 40, height: 70 }}
                            placeholder="Select your SIM"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selected2}
                            onValueChange={this.onValueChange2.bind(this)}
                        >
                            <Picker.Item label="Male" value="M" />
                            <Picker.Item label="Female" value="F" />

                        </Picker>
                    </Item>
                </Form>
                </View>
                <TouchableOpacity
                    onPress={this.registerUser} 
                    style={styles.btn}>
                    <Text>Log In</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const WIDTH = Dimensions.get('screen').width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputStyle: {
        width: WIDTH - 40,
        height: 70,
        borderRadius: 15,
        borderColor: 'lightgray',
        borderWidth: 0.5,
        marginTop: 10,
        paddingLeft: 10
    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        width: WIDTH - 40,
        height: 70,
        borderRadius: 15,
        backgroundColor: 'lightblue',
        marginTop: 10
    }
})


